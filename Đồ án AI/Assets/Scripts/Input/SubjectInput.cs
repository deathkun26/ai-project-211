using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubjectInput : NameInput
{
    override public void SetName(string name)
    {
        if (inputName == "None")
        {
            label.text = "Subject name " + name;
            inputField.placeholder.GetComponent<Text>().text = name;
            inputName = name;
        }
        else
            inputName = inputField.text;

        Manager.instance.subjectNameList[index] = inputName;
    }
}