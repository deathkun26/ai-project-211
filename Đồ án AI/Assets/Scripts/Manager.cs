using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Manager : MonoBehaviour
{
    public static Manager instance;
    public GameObject table;
    public GameObject initPanel;
    public GameObject tableContent;
    public GameObject tableHeader;
    public GameObject classColumnPrefab;
    public GameObject subjectRowPrefab;
    public GameObject graphUnitPrefab;
    public GameObject shadow;
    public GameObject buttonOk;
    public GameObject classInputPrefab;
    public GameObject subjectInputPrefab;
    public Transform classInputContainer;
    public Transform subjectInputContainer;
    public InputField numberClassInput;
    public InputField numberSubjectInput;
    [System.NonSerialized] public int numberClass = 0;
    [System.NonSerialized] public int numberSubject = 0;
    [System.NonSerialized] public bool[,] graph;
    public string[] classNameList;
    public string[] subjectNameList;
    public List<Color> colors;

    private void Start()
    {
        if (instance == null)
            instance = this;
        colors = new List<Color>();
        GenColors();
    }

    public void InitTable()
    {
        ClearContainer(tableContent.transform);
        shadow.SetActive(true);
        initPanel.SetActive(true);
    }

    public void ComfirmTable()
    {
        graph = new bool[numberSubject, numberClass];
        CreateTableUI();
        table.SetActive(true);
        shadow.SetActive(false);
        initPanel.SetActive(false);
    }

    public void SetNumberClass()
    {
        numberClass = int.Parse(numberClassInput.text);
        if (numberClass > 0 && numberSubject > 0)
            buttonOk.SetActive(true);
        ClearContainer(classInputContainer);

        classNameList = new string[numberClass];

        for (int i = 1; i <= numberClass; i++)
        {
            GameObject newClass = Instantiate(classInputPrefab, Vector3.zero, Quaternion.identity);
            newClass.GetComponent<ClassInput>().index = i - 1;
            if (i < 10)
                newClass.GetComponent<ClassInput>().SetName("L0" + i.ToString());
            else
                newClass.GetComponent<ClassInput>().SetName("L" + i.ToString());
            newClass.transform.SetParent(classInputContainer);
        }
    }

    public void SetNumberSubject()
    {
        numberSubject = int.Parse(numberSubjectInput.text);
        if (numberClass > 0 && numberSubject > 0)
            buttonOk.SetActive(true);
        ClearContainer(subjectInputContainer);

        subjectNameList = new string[numberSubject];

        for (int i = 1; i <= numberSubject; i++)
        {
            GameObject newSubject = Instantiate(subjectInputPrefab, Vector3.zero, Quaternion.identity);
            newSubject.GetComponent<SubjectInput>().index = i - 1;
            if (i < 10)
                newSubject.GetComponent<SubjectInput>().SetName("M0" + i.ToString());
            else
                newSubject.GetComponent<SubjectInput>().SetName("M" + i.ToString());
            newSubject.transform.SetParent(subjectInputContainer);
        }
    }

    void ClearContainer(Transform container)
    {
        for (int i = container.childCount - 1; i >= 0; i--)
        {
            Destroy(container.GetChild(i).gameObject);
        }
    }

    void CreateTableUI()
    {
        tableContent.GetComponent<GridLayoutGroup>().constraintCount = numberClass + 1;
        GameObject header = Instantiate(tableHeader, Vector3.zero, Quaternion.identity);
        header.transform.SetParent(tableContent.transform);
        for (int i = 0; i < numberClass; i++)
        {
            GameObject column = Instantiate(classColumnPrefab, Vector3.zero, Quaternion.identity);
            column.transform.SetParent(tableContent.transform);
            column.GetComponentInChildren<Text>().text = classNameList[i];
        }
        for (int j = 0; j < numberSubject; j++)
        {
            GameObject row = Instantiate(subjectRowPrefab, Vector3.zero, Quaternion.identity);
            row.transform.SetParent(tableContent.transform);
            row.GetComponentInChildren<Text>().text = subjectNameList[j];
            for (int i = 0; i < numberClass; i++)
            {
                GameObject unit = Instantiate(graphUnitPrefab, Vector3.zero, Quaternion.identity);
                unit.transform.SetParent(tableContent.transform);
                GraphUnit graphUnit = unit.GetComponentInChildren<GraphUnit>();
                graphUnit.col = i;
                graphUnit.row = j;
            }
        }
    }

    void GenColors(float r = 1, float g = 1, float b = 1)
    {

        // r g b
        colors.Add(new Color(r, 0, 0));
        colors.Add(new Color(0, g, 0));
        colors.Add(new Color(0, 0, b));
        colors.Add(new Color(r, g, 0));
        colors.Add(new Color(r, 0, b));
        colors.Add(new Color(0, g, b));

        // r g/2 b , r g b/2 , r/2 g b
        colors.Add(new Color(r, g / 2, 0));
        colors.Add(new Color(0, g, b / 2));
        colors.Add(new Color(r / 2, 0, b));
        colors.Add(new Color(r, g, b / 2));
        colors.Add(new Color(r, g / 2, b));
        colors.Add(new Color(r / 2, g, b));

        // r/2 g/2 b/2
        colors.Add(new Color(r / 2, 0, 0));
        colors.Add(new Color(0, g / 2, 0));
        colors.Add(new Color(0, 0, b / 2));
        colors.Add(new Color(r / 2, g / 2, 0));
        colors.Add(new Color(r / 2, 0, b / 2));
        colors.Add(new Color(0, g / 2, b / 2));
        colors.Add(new Color(r / 2, g / 2, b / 2));

        // r*0.75 - g*0.75 - b*0.75
        colors.Add(new Color(r * 0.75f, 0, 0));
        colors.Add(new Color(0, g * 0.75f, 0));
        colors.Add(new Color(0, 0, b * 0.75f));
        colors.Add(new Color(r * 0.75f, g * 0.75f, 0));
        colors.Add(new Color(r * 0.75f, 0, b * 0.75f));
        colors.Add(new Color(0, g * 0.75f, b * 0.75f));
        colors.Add(new Color(r * 0.75f, g * 0.75f, b * 0.75f));

        // r/4 g/4 b/4
        colors.Add(new Color(r / 4, 0, 0));
        colors.Add(new Color(0, g / 4, 0));
        colors.Add(new Color(0, 0, b / 4));
        colors.Add(new Color(r / 4, g / 4, 0));
        colors.Add(new Color(r / 4, 0, b / 4));
        colors.Add(new Color(0, g / 4, b / 4));
        colors.Add(new Color(r / 4, g / 4, b / 4));

        // r*0.625 - g*0.625 - b*0.625
        colors.Add(new Color(r * 0.625f, 0, 0));
        colors.Add(new Color(0, g * 0.625f, 0));
        colors.Add(new Color(0, 0, b * 0.625f));
        colors.Add(new Color(r * 0.625f, g * 0.625f, 0));
        colors.Add(new Color(r * 0.625f, 0, b * 0.625f));
        colors.Add(new Color(0, g * 0.625f, b * 0.625f));
        colors.Add(new Color(r * 0.625f, g * 0.625f, b * 0.625f));

        // r*0.865 - g*0.865 - b*0.865
        colors.Add(new Color(r * 0.865f, 0, 0));
        colors.Add(new Color(0, g * 0.865f, 0));
        colors.Add(new Color(0, 0, b * 0.865f));
        colors.Add(new Color(r * 0.865f, g * 0.865f, 0));
        colors.Add(new Color(r * 0.865f, 0, b * 0.865f));
        colors.Add(new Color(0, g * 0.865f, b * 0.865f));
        colors.Add(new Color(r * 0.865f, g * 0.865f, b * 0.865f));

        // r*0.375 - g*0.375 - b*0.375
        colors.Add(new Color(r * 0.375f, 0, 0));
        colors.Add(new Color(0, g * 0.375f, 0));
        colors.Add(new Color(0, 0, b * 0.375f));
        colors.Add(new Color(r * 0.375f, g * 0.375f, 0));
        colors.Add(new Color(r * 0.375f, 0, b * 0.375f));
        colors.Add(new Color(0, g * 0.375f, b * 0.375f));
        colors.Add(new Color(r * 0.375f, g * 0.375f, b * 0.375f));

    }
}
