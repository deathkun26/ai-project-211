using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GraphUnit : MonoBehaviour
{
    public Toggle toggle;
    public int col;
    public int row;
    public void UpdateGraph()
    {
        Manager.instance.graph[row, col] = toggle.isOn;
    }
}
