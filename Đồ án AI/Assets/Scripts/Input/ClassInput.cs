using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ClassInput : NameInput
{
    override public void SetName(string name)
    {
        if (inputName == "None")
        {
            label.text = "Class name " + name;
            inputField.placeholder.GetComponent<Text>().text = name;
            inputName = name;
        }
        else
            inputName = inputField.text;

        Manager.instance.classNameList[index] = inputName;
    }
}