using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Coloring : MonoBehaviour
{
    public Text timeText;
    public List<GameObject> buttonList;
    public GameObject confirmButton;
    public static Coloring instance;
    public List<List<int>> caThi;
    public GameObject demoPanel;
    public float radius;
    public float firstAngle;
    public Transform center;
    public GameObject pointPrefab;
    public Transform pointContainer;
    public GameObject linePrefab;
    public Transform lineContainer;
    public GameObject caThiPrefab;
    public Transform caThiContainer;
    public GameObject statePanel;
    public float waitTime;
    private List<GameObject> points;
    private float startTime;
    private bool running = false;
    private void Start()
    {
        if (instance == null)
            instance = this;
    }

    private void Update()
    {
        if (running)
        {
            timeText.text = "Time : " + (Time.time - startTime).ToString();
        }
    }

    #region Graph
    public List<List<int>> GraphForGreedy(bool[,] originGraph)
    {
        List<List<int>> graph = new List<List<int>>();
        int n = Manager.instance.numberSubject;
        int m = Manager.instance.numberClass;

        //Init a row for each subject -> Add to graph
        for (int r = 0; r < n; r++)
        {
            List<int> row = new List<int>();
            graph.Add(row);
        }

        List<List<int>> col = new List<List<int>>();
        for (int c = 0; c < m; c++)
        {
            col.Add(new List<int>());
            // Get all the subjects of each class
            for (int i = 0; i < n; i++)
            {
                if (originGraph[i, c])
                    col[c].Add(i);

            }

            for (int i = 0; i < col[c].Count; i++)
                for (int j = 0; j < col[c].Count; j++)
                {
                    if (graph[col[c][i]].Contains(col[c][j]) || i == j)
                        continue;
                    graph[col[c][i]].Add(col[c][j]);
                    graph[col[c][j]].Add(col[c][i]);
                }
        }

        return graph;
    }
    public List<List<int>> NormalGraph(bool[,] originGraph)
    {
        List<List<int>> graph = new List<List<int>>();
        int n = Manager.instance.numberSubject;
        int m = Manager.instance.numberClass;

        // Init graph
        for (int i = 0; i < n; i++)
        {
            graph.Add(new List<int>());
            for (int j = 0; j < n; j++)
            {
                graph[i].Add(0);
            }
        }

        List<List<int>> col = new List<List<int>>();
        for (int c = 0; c < m; c++)
        {
            col.Add(new List<int>());
            // Get all the subjects of each class
            for (int i = 0; i < n; i++)
            {
                if (originGraph[i, c])
                    col[c].Add(i);

            }
            for (int i = 0; i < col[c].Count; i++)
                for (int j = 0; j < col[c].Count; j++)
                {
                    if (col[c][i] == col[c][j])
                        continue;
                    graph[col[c][i]][col[c][j]] = 1;
                    graph[col[c][j]][col[c][i]] = 1;
                }
        }
        // for (int i = 0; i < n; i++)
        // {
        //     string s = "";
        //     for (int j = 0; j < n; j++)
        //     {
        //         s += graph[i][j].ToString() + "\t";
        //     }
        //     Debug.Log(s);
        // }
        return graph;
    }
    #endregion


    #region Trigger
    public void GreedyTrigger()
    {
        running = true;
        startTime = Time.time;
        StartCoroutine(Greedy());
    }

    public void GeneticTrigger()
    {
        running = true;
        startTime = Time.time;
        StartCoroutine(Genetic());
    }

    public void WelshPowellTrigger()
    {
        running = true;
        startTime = Time.time;
        StartCoroutine(WelshPowell());
    }
    #endregion



    #region Algorithms
    // Greedy
    IEnumerator Greedy()
    {
        //Start
        DrawGraph(NormalGraph(Manager.instance.graph));
        ClearContainer(caThiContainer);
        //Coloring using Greedy
        int V = Manager.instance.numberSubject;
        List<List<int>> adj = GraphForGreedy(Manager.instance.graph);
        List<int> result = new List<int>();
        List<bool> available = new List<bool>();

        // Init result and available list
        for (int i = 0; i < V; i++)
        {
            result.Add(-1);
            available.Add(false);
        }
        yield return null;
        // Assign the first color to first vertex
        result[0] = 0;
        points[0].GetComponentInChildren<Image>().color = Manager.instance.colors[0];
        yield return new WaitForSeconds(waitTime);
        // Assign colors to remaining V-1 vertices

        for (int u = 1; u < V; u++)
        {
            // Process all adjacent vertices and
            // flag their colors as unavailable
            foreach (int i in adj[u])
            {
                if (result[i] != -1)
                    available[result[i]] = true;
            }

            //  Find the first available color
            int cr = 0;

            while (cr < V)
            {
                if (available[cr] == false)
                    break;
                cr += 1;
            }

            // Assign the found color
            result[u] = cr;
            points[u].GetComponentInChildren<Image>().color = Manager.instance.colors[cr];
            yield return new WaitForSeconds(waitTime);

            // Reset the values back to false
            // for the next iteration
            foreach (int i in adj[u])
            {
                if (result[i] != -1)
                    available[result[i]] = false;
            }
        }

        ShowDemo(result, "Greedy");
    }



    // Genetic
    IEnumerator Genetic()
    {
        //Start
        List<List<int>> graph = NormalGraph(Manager.instance.graph);
        DrawGraph(graph);
        ClearContainer(caThiContainer);

        //Init
        int n = Manager.instance.numberSubject;
        bool condition = true;

        int numberOfColors = MaxNumColor(graph);
        Debug.Log(numberOfColors);
        yield return new WaitForSeconds(2.0f);
        int crt_best_fitness = numberOfColors;
        List<int> crt_fittest_individual = new List<int>();

        while (condition && numberOfColors > 0)
        {

            int population_size = 200;

            List<List<int>> population = new List<List<int>>();
            for (int i = 0; i < population_size; i++)
            {
                List<int> individual = CreateIndividual(n, numberOfColors);
                population.Add(new List<int>(individual));
            }

            int best_fitness = Fitness(graph, population[0], n);
            List<int> fittest_individual = new List<int>(population[0]);
            int gen = 0;

            while (best_fitness != 0 && gen != 10000)
            {
                gen += 1;

                RouletteWheelSelection(ref population, graph, n);

                List<List<int>> new_population = new List<List<int>>();
                Shuffle(population);

                for (int i = 0; i < population_size - 1; i += 2)
                {

                    List<int> child1 = new List<int>();
                    List<int> child2 = new List<int>();
                    Crossover(population[i], population[i + 1], ref child1, ref child2, n);
                    new_population.Add(new List<int>(child1)); // child1
                    new_population.Add(new List<int>(child2)); // child2

                }

                for (int i = 0; i < new_population.Count; i++)
                {
                    if (gen < 200)
                        mutation1(new_population[i], n, numberOfColors);
                    else
                        mutation2(new_population[i], n, numberOfColors);
                }

                population = new List<List<int>>(new_population);
                best_fitness = Fitness(graph, population[0], n);
                fittest_individual = new List<int>(population[0]);

                foreach (var individual in population)
                {
                    if (Fitness(graph, individual, n) < best_fitness)
                    {
                        best_fitness = Fitness(graph, individual, n);
                        fittest_individual = new List<int>(individual);
                    }
                }

                if (gen % 10 == 0)
                    Debug.Log("Generation: " + gen + " Best_Fitness: " + best_fitness + " Individual: " + ConvertToString(fittest_individual));

                for (int i = 0; i < fittest_individual.Count; i++)
                {
                    points[i].GetComponentInChildren<Image>().color = Manager.instance.colors[fittest_individual[i] - 1];
                }
                yield return new WaitForSeconds(0.01f);
            }

            Debug.Log("Using " + numberOfColors + " colors : ");
            Debug.Log("Generation: " + gen + " Best_Fitness: " + best_fitness + " Individual: " + ConvertToString(fittest_individual));
            Debug.Log(" -\t-\t-");

            if (best_fitness != 0)
            {
                condition = false;
                Debug.Log("Graph is " + (numberOfColors + 1) + " colorable");
            }
            else
            {
                crt_best_fitness = best_fitness;
                crt_fittest_individual = new List<int>(fittest_individual);
                numberOfColors--;
            }

            yield return new WaitForSeconds(waitTime);
        }

        Debug.Log("Finish");

        List<int> result = new List<int>();
        foreach (var i in crt_fittest_individual)
            result.Add(i - 1);

        for (int i = 0; i < crt_fittest_individual.Count; i++)
        {
            points[i].GetComponentInChildren<Image>().color = Manager.instance.colors[crt_fittest_individual[i] - 1];
        }

        ShowDemo(result, "Genetic");
    }
    int Sum(List<int> array)
    {
        int result = 0;
        foreach (int value in array)
            result += value;
        return result;
    }
    int MaxNumColor(List<List<int>> graph)
    {
        int max_num_colors = 1;
        for (int i = 0; i < graph.Count; i++)
        {
            int sum = Sum(graph[i]);
            if (sum + 1 > max_num_colors)
                max_num_colors = sum + 1;
        }
        return max_num_colors;
    }
    List<int> CreateIndividual(int n, int numberOfColors)
    {
        List<int> individual = new List<int>();

        for (int i = 0; i < n; i++)
            individual.Add(Random.Range(1, numberOfColors + 1));

        return individual;
    }
    int Fitness(List<List<int>> graph, List<int> individual, int n)
    {
        int fitness = 0;

        for (int i = 0; i < n; i++)
            for (int j = i; j < n; j++)
                if (individual[i] == individual[j] && graph[i][j] == 1)
                    fitness += 1;

        return fitness;
    }
    void Crossover(List<int> parent1, List<int> parent2, ref List<int> child1, ref List<int> child2, int n)
    {
        int position = Random.Range(2, n - 2 + 1);
        child1 = new List<int>();
        child2 = new List<int>();
        for (int i = 0; i < position + 1; i++)
        {
            child1.Add(parent1[i]);
            child2.Add(parent2[i]);
        }
        for (int i = position + 1; i < n; i++)
        {
            child1.Add(parent2[i]);
            child2.Add(parent1[i]);
        }
    }
    void mutation1(List<int> individual, int n, int number_of_colors)
    {
        float probability = 0.4f;
        float check = Random.Range(0, 1.0f);

        if (check <= probability)
        {
            int position = Random.Range(0, n);

            individual[position] = Random.Range(1, number_of_colors + 1);
        }
    }

    void mutation2(List<int> individual, int n, int number_of_colors)
    {
        float probability = 0.2f;
        float check = Random.Range(0, 1.0f);

        if (check <= probability)
        {
            int position = Random.Range(0, n);

            individual[position] = Random.Range(1, number_of_colors + 1);
        }
    }

    void Shuffle(List<List<int>> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var i0 = Random.Range(0, count);
            var i1 = Random.Range(0, count);
            Swap<int>(ts[i0], ts[i1]);
        }
    }
    void Swap<T>(IList<T> a, IList<T> b)
    {
        int n = a.Count;
        for (int i = 0; i < n; i++)
        {
            var temp = a[i];
            a[i] = b[i];
            b[i] = temp;
        }
    }
    void RouletteWheelSelection(ref List<List<int>> population, List<List<int>> graph, int n)
    {
        double total_fitness = 0;

        foreach (var individual in population)
            total_fitness += Fitness(graph, individual, n);

        List<double> cumulative_fitness = new List<double>();
        double cumulative_fitness_sum = 0;

        for (int i = 0; i < population.Count; i++)
        {
            cumulative_fitness_sum += Fitness(graph, population[i], n);
            cumulative_fitness.Add(cumulative_fitness_sum);
        }

        List<List<int>> new_population = new List<List<int>>();

        for (int i = 0; i < population.Count; i++)
        {
            double roulette = Random.Range(0, 1.0f);
            for (int j = 0; j < population.Count; j++)
            {
                if (roulette * total_fitness <= cumulative_fitness[j])
                {
                    new_population.Add(new List<int>(population[j]));
                    break;
                }
            }

        }
        if (new_population.Count < population.Count)
            Debug.Log("Error");

        population = new List<List<int>>(new_population);
    }
    string ConvertToString(List<int> list)
    {
        string s = "[ ";
        s += list[0].ToString();
        for (int i = 1; i < list.Count; i++)
            s += ", " + list[i].ToString();
        s += " ]";
        return s;
    }


    // Welsh Powell
    IEnumerator WelshPowell()
    {
        //Start

        List<List<int>> graph = NormalGraph(Manager.instance.graph);
        DrawGraph(graph);
        ClearContainer(caThiContainer);

        int V = Manager.instance.numberSubject;
        List<int> colour = new List<int>();
        for (int i = 0; i < V; i++)
            colour.Add(0);

        List<bool> finish = new List<bool>();
        finish.Add(false);
        StartCoroutine(WelshPowellRecursion(V, graph, SortedGraph(graph, V), colour, finish));

        while (finish[0] == false)
        {
            yield return null;
        }

        // Demo
        List<int> result = new List<int>();
        for (int i = 0; i < colour.Count; i++)
            result.Add(colour[i] - 1);

        ShowDemo(result, "WelshPowell");

    }
    IEnumerator WelshPowellRecursion(int V, List<List<int>> graph, List<List<int>> sortedGraph, List<int> colour, List<bool> finish, int c = 1)
    {
        if (!colour.Contains(0))
        {
            finish[0] = true;
        }
        else
        {
            colour[sortedGraph[0][V]] = c;
            points[sortedGraph[0][V]].GetComponentInChildren<Image>().color = Manager.instance.colors[c - 1];
            yield return new WaitForSeconds(waitTime);
            for (int i = 1; i < sortedGraph.Count; i++)
            {
                if (IsSafe(graph, colour, sortedGraph[i][V], c))
                {
                    colour[sortedGraph[i][V]] = c;
                    points[sortedGraph[i][V]].GetComponentInChildren<Image>().color = Manager.instance.colors[c - 1];
                    yield return new WaitForSeconds(waitTime);
                }
            }
            List<List<int>> newSortedGraph = new List<List<int>>();
            for (int i = 0; i < sortedGraph.Count; i++)
            {
                List<int> row = new List<int>(sortedGraph[i]);
                if (colour[sortedGraph[i][V]] == 0)
                    newSortedGraph.Add(row);
            }

            StartCoroutine(WelshPowellRecursion(V, graph, newSortedGraph, colour, finish, c + 1));
        }
    }
    List<List<int>> SortedGraph(List<List<int>> graph, int V)
    {
        List<List<int>> result = new List<List<int>>(graph);
        List<int> sum = new List<int>();
        for (int i = 0; i < V; i++)
        {
            sum.Add(Sum(result[i]));
            result[i].Add(i); // index of subject
        }
        for (int i = 0; i < V - 1; i++)
        {
            for (int j = i; j < V; j++)
                if (sum[j] > sum[i])
                {
                    List<int> temp = result[j];
                    result[j] = result[i];
                    result[i] = temp;
                }
        }

        return result;
    }
    bool IsSafe(List<List<int>> graph, List<int> colour, int v, int c)
    {
        for (int i = 0; i < colour.Count; i++)
        {
            if (graph[v][i] == 1 && colour[i] == c)
                return false;
        }
        return true;
    }
    #endregion



    #region Demo
    void DrawGraph(List<List<int>> graph)
    {
        // Init
        int n = Manager.instance.numberSubject;
        points = new List<GameObject>();
        ClearContainer(pointContainer);
        // First Point
        GameObject point = Instantiate(pointPrefab, center.transform.position, Quaternion.identity);
        point.transform.SetParent(pointContainer);
        points.Add(point);
        float x = center.position.x + radius * Mathf.Cos(firstAngle * Mathf.Deg2Rad);
        float y = center.position.y + radius * Mathf.Sin(firstAngle * Mathf.Deg2Rad);
        Vector2 firstPos = new Vector2(x, y);
        point.transform.position = firstPos;
        point.GetComponentInChildren<Text>().text = Manager.instance.subjectNameList[0];

        // Create n-1 Point
        float w = 360.0f / n;
        float angle = firstAngle;
        for (int i = 1; i < n; i++)
        {
            angle += w;
            Vector2 crtPos = NextPos((angle) * Mathf.Deg2Rad, center.transform.position);
            GameObject newPoint = Instantiate(pointPrefab, center.transform.position, Quaternion.identity);
            newPoint.transform.SetParent(pointContainer);
            newPoint.transform.position = crtPos;
            newPoint.GetComponentInChildren<Text>().text = Manager.instance.subjectNameList[i];
            points.Add(newPoint);
        }

        // Draw lines
        ClearContainer(lineContainer);
        for (int i = 0; i < n - 1; i++)
        {
            for (int j = i + 1; j < n; j++)
            {
                if (graph[i][j] == 1)
                {
                    DrawLine(points[i], points[j]);
                }
            }
        }

        demoPanel.SetActive(true);
    }
    Vector2 NextPos(float w, Vector2 crt)
    {
        return new Vector2(crt.x + radius * Mathf.Cos(w), crt.y + radius * Mathf.Sin(w));
    }
    void ShowDemo(List<int> result, string algorithms)
    {
        running = false;
        // Create "caThi"
        caThi = new List<List<int>>();
        int maxCaThi = result[0];
        for (int i = 0; i < result.Count; i++)
        {
            maxCaThi = maxCaThi > result[i] ? maxCaThi : result[i];
            Debug.Log(algorithms + " : Mon " + Manager.instance.subjectNameList[i] + " ---> Thi ca " + (result[i] + 1));
        }
        for (int i = 0; i < maxCaThi + 1; i++)
        {
            caThi.Add(new List<int>());
        }
        for (int i = 0; i < result.Count; i++)
        {
            caThi[result[i]].Add(i);
        }

        confirmButton.SetActive(true);
        // Create Subject list for each exam
        for (int i = 0; i < caThi.Count; i++)
        {
            GameObject newCaThi = Instantiate(caThiPrefab, Vector3.zero, Quaternion.identity);
            newCaThi.transform.SetParent(caThiContainer);
            newCaThi.GetComponentInChildren<Text>().text = "Ca thi " + (i + 1).ToString();
            foreach (int j in caThi[i])
            {
                newCaThi.GetComponent<CaThi>().AddSubject(Instantiate(points[j]));
            }
        }
    }
    void DrawLine(GameObject pointStart, GameObject pointEnd)
    {
        Vector2 start = pointStart.transform.position;
        Vector2 end = pointEnd.transform.position;

        Vector2 direction = end - start;
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg - 90.0f;
        GameObject line = Instantiate(linePrefab, Vector3.zero, Quaternion.Euler(0, 0, angle));
        line.transform.SetParent(lineContainer);
        line.transform.position = (end + start) / 2;
        line.GetComponent<RectTransform>().sizeDelta = new Vector2(3, direction.magnitude);
    }
    void ClearContainer(Transform container)
    {
        for (int i = container.childCount - 1; i >= 0; i--)
        {
            Destroy(container.GetChild(i).gameObject);
        }
    }
    public void CloseDemo()
    {
        confirmButton.SetActive(false);
        demoPanel.SetActive(false);
    }
    #endregion

    public void Active()
    {
        foreach (GameObject button in buttonList)
        {
            button.SetActive(true);
        }
    }
}
