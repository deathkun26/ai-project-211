using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaThi : MonoBehaviour
{
    public Transform subjectContainer;
    public void AddSubject(GameObject point)
    {
        point.transform.SetParent(subjectContainer);
    }
}
