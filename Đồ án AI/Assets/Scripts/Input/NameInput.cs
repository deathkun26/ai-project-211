using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class NameInput : MonoBehaviour
{
    public int index;
    public Text label;
    public InputField inputField;
    public string inputName = "None";

    virtual public void SetName(string name) { }

}
